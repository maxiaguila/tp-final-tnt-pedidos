package com.example.tp_final_tnt_2

import androidx.lifecycle.ViewModel
import com.google.firebase.database.DataSnapshot

class RestaurarViandaViewModel: ViewModel() {
    var viandas = emptyArray<Vianda>()
    var viandaElegida: Vianda = Vianda()

    fun addVianda(viandaSnapshot: DataSnapshot) {
        val vianda = getViandaFromDataSnapshot(viandaSnapshot)
        if (viandas.contains(vianda)) return
        this.viandas = this.viandas.plus(vianda)
    }

    private fun getViandaFromDataSnapshot(viandaSnapshot: DataSnapshot): Vianda {
        val nombre = viandaSnapshot.child("nombre").getValue(String::class.java).toString()
        val descripcion = viandaSnapshot.child("descripcion").getValue(String::class.java).toString()
        val precio= viandaSnapshot.child("precio").getValue(String::class.java).toString()
        val fecha = viandaSnapshot.child("fecha").getValue(String::class.java).toString()
        val uri = viandaSnapshot.child("uri").getValue(String::class.java).toString()
        return Vianda(id = viandaSnapshot.key, nombre = nombre, descripcion = descripcion, precio = precio, uri = uri)
    }
}