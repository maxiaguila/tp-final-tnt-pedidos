package com.example.tp_final_tnt_2

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.tp_final_tnt_2.databinding.FragmentRegistroPlatoDestBinding
import kotlinx.android.synthetic.main.fragment_registro_plato_dest.*
import java.text.SimpleDateFormat
import java.util.*
import com.google.firebase.storage.FirebaseStorage
import android.widget.Toast
import android.content.Intent
import android.app.Activity
import android.app.ProgressDialog
import android.net.Uri
import android.provider.MediaStore
import android.widget.ArrayAdapter
import androidx.core.text.set
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.dialogo_ingrdiente.*
import kotlinx.android.synthetic.main.dialogo_ingrdiente.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [registro_plato_dest.newInstance] factory method to
 * create an instance of this fragment.
 */
class registro_plato_dest : Fragment() {

    private lateinit var _binding: FragmentRegistroPlatoDestBinding
    private val binding get() = _binding!!
    private lateinit var database: DatabaseReference
    private val REQUEST_CODE = 1000
    lateinit var imageUri: Uri
    private lateinit var materialAlertDialogBuilder: MaterialAlertDialogBuilder
    private lateinit var modalIngredientesView: View
    // contiene la lista de ingredientes a mostrar en el spinner
    private var lista_ingredienes = mutableListOf<Ingrediente>()
    // contiene la lista de ingredientes seleccionados para agregar al plato
    private var ingredienes_para_agregar_al_plato = mutableListOf<IngredientePlato>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        llenar_lista_ingredientes_spinner()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragmentz
        this._binding = FragmentRegistroPlatoDestBinding.inflate(inflater, container, false)
        val  view = binding.root

        materialAlertDialogBuilder = MaterialAlertDialogBuilder(context)

        binding.registroPlato.setOnClickListener { registrarPlatoEnBD() }
        binding.seleccionarImagen.setOnClickListener { openGaleria() }
        binding.btnAddNewIngredient.setOnClickListener { agregar_ingrediente() }
        binding.btnVerIngredientesPlato.setOnClickListener { ver_ingredientes_para_agregar_al_plato() }
        return view
    }

    private fun ver_ingredientes_para_agregar_al_plato() {
        var items:MutableList<String> = mutableListOf()
        ingredienes_para_agregar_al_plato.forEach { it -> items.add("${it.name} (${it.quantity})") }
        var dialog = MaterialAlertDialogBuilder(context)
            .setTitle("Ingredientes del plato")
            .setPositiveButton("Cerrar") { dialog, _ ->
                dialog.dismiss()
            }
        if (items.isEmpty()) {
            dialog.setMessage("No hay ingredientes cargados")
        } else {
            dialog.setItems(items.toTypedArray()) { _, _ -> }
        }
        dialog.show()
    }

    private fun agregar_ingrediente() {
        modalIngredientesView = LayoutInflater.from(context).inflate(R.layout.dialogo_ingrdiente,null,false)
        agregar_ingredientes_a_spinner()
        //var ingrediente = modalIngredientesView.spinner_ingredientes
        var cantidad =  modalIngredientesView.cant_ingredientes_plato
        materialAlertDialogBuilder.setView(modalIngredientesView)
            .setTitle("Agregar un nuevo ingrediente")
            .setPositiveButton("Agregar") { dialog, _ ->
                val item : Any = modalIngredientesView.spinner_ingredientes.selectedItem
                val i = item as Ingrediente
                val cant = cantidad.cant_ingredientes_plato.text.toString().toInt()
                val ingrediente = IngredientePlato(id = i.id, name = i.name, quantity = cant)

                Log.d("ingredientes", ingrediente.toString())
                ingredienes_para_agregar_al_plato.add(ingrediente)

                dialog.dismiss()
            }
            .setNegativeButton("Cancelar") { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }


    private fun agregar_ingredientes_a_spinner(){
        var adapter: ArrayAdapter<Ingrediente> = ArrayAdapter(
            requireContext(),android.R.layout.simple_spinner_item,lista_ingredienes)

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        modalIngredientesView.spinner_ingredientes.adapter = adapter


    }

    private fun llenar_lista_ingredientes_spinner() {
        database = FirebaseDatabase.getInstance().reference
        database.child("ingredientes").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("DBFirebase", "Error de lectura: $databaseError")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    dataSnapshot.children.forEach {
                        //var name = it.child("name").value.toString()
                        val ingrediente = Ingrediente(
                            name = it.child("name").value.toString() ,
                            description = it.child("description").value.toString(),
                            quantity = it.child("quantity").value.toString().toInt(),
                            minimun_stock_level = it.child("minimun_stock_level").value.toString().toInt(),
                            id = it.child("id").value.toString()
                        )
                        lista_ingredienes.add( ingrediente )
                    }
                }
            }
        })
    }

    private fun registrarPlatoEnBD() {
        if (sonCamposPlatoIncorrectos()) return
        when (radioGroup.getCheckedRadioButtonId()) {
            R.id.vianda -> registrarViandaEnBD()
            else -> registarPlatoEnMenus()
        }
        navegarHaciaPrincipal()
    }

    private fun sonCamposPlatoIncorrectos():Boolean {
        val nombre = binding.nombrePlato.text.toString()
        val descripcion = binding.descripcionPlato.text.toString()
        val precio = binding.precio.text.toString()

        if(nombre.isNullOrEmpty() or descripcion.isNullOrEmpty() or precio.isNullOrEmpty()){
            Toast.makeText(context, "Los campos son obligatorios", Toast.LENGTH_SHORT).show()
            return true
        }
        if (ingredienes_para_agregar_al_plato.isEmpty()) {
            Toast.makeText(context, "Debes agrear al menos un ingrediente", Toast.LENGTH_SHORT).show()
            return true
        }
        if (precio.contains(",", true)) {
            Toast.makeText(context, "Antes de los decimales debe ir un punto", Toast.LENGTH_SHORT).show()
            return true
        }
        return false
    }

    private fun registrarViandaEnBD() {
        database = FirebaseDatabase.getInstance().reference
        val vianda = armarVianda()
        val key = database.child("viandas").push().key
        vianda.id = key
        if (key == null) {
            Log.i("BDFirebase", "No pude pushear clave de menus")
            return
        }

        database.child("viandas").child(key).setValue(vianda)
            .addOnFailureListener{
                Toast.makeText(context, "No se pudo registrar la Vianda", Toast.LENGTH_SHORT).show()
            }
            .addOnSuccessListener {
                Toast.makeText(context, "Vianda cargada correctamente", Toast.LENGTH_SHORT).show()
                var ref = database.child("viandas").child(key).child("ingredientes")
                uploadIngredientes(ref)
                uploadImage(key)
            }
    }

    private fun registarPlatoEnMenus() {
        database = FirebaseDatabase.getInstance().reference
        val plato = armarPlato()
        val key = database.child("menus").child(plato.tipo.toString()).push().key
        val tipoPlato = plato.tipo.toString()
        plato.id = key
        if (key == null) {
            Log.i("BDFirebase", "No pude pushear clave de menus")
            return
        }

        database.child("menus").child(tipoPlato).child(key).setValue(plato)
            .addOnFailureListener{
                Toast.makeText(context, "No se pudo registrar el Plato", Toast.LENGTH_SHORT).show()
            }
            .addOnSuccessListener {
                Toast.makeText(context, "Plato cargado correctamente", Toast.LENGTH_SHORT).show()
                var ref = database.child("menus").child(tipoPlato).child(key).child("ingredientes")
                uploadIngredientes(ref)
                uploadImage(key) // Subo Imagen a Firebase Storage
            }
    }

    private fun uploadIngredientes(reference: DatabaseReference) {
        ingredienes_para_agregar_al_plato.forEach { ingrediente ->
            reference.child(ingrediente.id.toString()).setValue(ingrediente).addOnSuccessListener {
                Toast.makeText(context,"Ingrediente agregado: ${ingrediente}",Toast.LENGTH_SHORT)
            }
        }
    }

    private fun armarVianda(): Vianda {
        val nombre = binding.nombrePlato.text.toString()
        val descripcion = binding.descripcionPlato.text.toString()
        val precio = binding.precio.text.toString()
        var fecha = ""
        var dia = ""
        val uri = imageUri.toString()

        return  Vianda(nombre, descripcion, precio, fecha, dia, uri)
    }

    private fun armarPlato() :Plato {
        val nombre = _binding.nombrePlato.text.toString()
        val descripcion = _binding.descripcionPlato.text.toString()
        val precio = _binding.precio.text.toString()
        var tipo = ""
        val radioGroup = _binding.radioGroup
        val fecha = obtenerFecha()
        when (radioGroup.getCheckedRadioButtonId()){
            R.id.desayuno -> tipo = "desayuno"
            R.id.almuerzo -> tipo = "almuerzo"
            R.id.merienda -> tipo = "merienda"
        }

        return Plato(nombre, descripcion, precio, tipo, fecha)
    }

    private fun obtenerFecha():String {
        val sdf = SimpleDateFormat("dd/M/yyyy", Locale.getDefault())
        val currentDate = sdf.format(Date())

        return currentDate
    }

    private fun navegarHaciaPrincipal() {
        findNavController().navigate(R.id.action_registro_plato_dest2_to_pedidos_dest)
    }

    private fun openGaleria() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        startActivityForResult(
            Intent.createChooser(intent, "Seleccionar imagen"), REQUEST_CODE)
    }

    private fun uploadImage(id: String) {
        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Subiendo archivo ...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        val storageReference = FirebaseStorage.getInstance().getReference("images/$id")

        storageReference?.putFile(imageUri!!).
        addOnSuccessListener {
            binding.avatarPlato.setImageURI(null)
            Toast.makeText(context, "La carga fue exitosa ! :)", Toast.LENGTH_SHORT).show()
            if (progressDialog.isShowing) progressDialog.dismiss()

        }.addOnFailureListener{

            if (progressDialog.isShowing) progressDialog.dismiss()
            Toast.makeText(context, "ERROR :(", Toast.LENGTH_SHORT).show()

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE){
            imageUri = data?.data!!
            binding.avatarPlato.setImageURI(imageUri)
            Toast.makeText(context, "Imagen Seleccionada !", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment registro_plato_dest.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            registro_plato_dest().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
