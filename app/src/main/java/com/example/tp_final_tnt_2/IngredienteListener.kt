package com.example.tp_final_tnt_2

interface IngredienteListener {
    fun onClickIngrediente(ingrediente: Ingrediente)
}