package com.example.tp_final_tnt_2
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.tp_final_tnt_2.databinding.FragmentEliminarPlatoDestBinding
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.fragment_eliminar_plato_dest.*
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [registro_plato_dest.newInstance] factory method to
 * create an instance of this fragment.
 */
class eliminar_plato_dest : Fragment() {

    private lateinit var _binding: FragmentEliminarPlatoDestBinding
    private val binding get() = _binding!!
    private lateinit var database: DatabaseReference
    private val menuDiarioViewModel: MenuDiarioViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        cargarMenusAlmuerzo()
        cargarMenusDesayuno()
        cargarMenusMeriendas()
        cargarViandas()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this._binding = FragmentEliminarPlatoDestBinding.inflate(inflater, container, false)
        initSpiner()

        binding.mostrarFotoPlato.setOnClickListener { mostrarPlato() }
        binding.eliminarButton.setOnClickListener { eliminarPlato() }

        return binding.root
    }

    private fun eliminarPlato () {
        val idPlato : String = menuDiarioViewModel.platoElegido.id.toString()
        var tipoPlato : String = menuDiarioViewModel.platoElegido.tipo.toString()
        if (tipoPlato == "null") {
            tipoPlato = "viandas"
        }
        database = FirebaseDatabase.getInstance().reference

        if (tipoPlato == "almuerzo") {
            database.child("menus").child("almuerzo").child(idPlato).removeValue()
                .addOnFailureListener{
                    Toast.makeText(context, "No se pudo Eliminar el Plato", Toast.LENGTH_SHORT).show()
                }
                .addOnSuccessListener {
                    Toast.makeText(context, "Plato Eliminado correctamente", Toast.LENGTH_SHORT).show()
                }
        }

        if (tipoPlato == "desayuno") {
            database.child("menus").child("desayuno").child(idPlato).removeValue()
                .addOnFailureListener{
                    Toast.makeText(context, "No se pudo Eliminar el Plato", Toast.LENGTH_SHORT).show()
                }
                .addOnSuccessListener {
                    Toast.makeText(context, "Plato Eliminado correctamente", Toast.LENGTH_SHORT).show()
                }
        }

        if (tipoPlato == "merienda") {
            database.child("menus").child("merienda").child(idPlato).removeValue()
                .addOnFailureListener{
                    Toast.makeText(context, "No se pudo Eliminar el Plato", Toast.LENGTH_SHORT).show()
                }
                .addOnSuccessListener {
                    Toast.makeText(context, "Plato Eliminado correctamente", Toast.LENGTH_SHORT).show()
                }
        }

        if (tipoPlato == "viandas") {
            database.child("viandas").child(idPlato).removeValue()
                .addOnFailureListener{
                    Toast.makeText(context, "No se pudo Eliminar el Plato", Toast.LENGTH_SHORT).show()
                }
                .addOnSuccessListener {
                    Toast.makeText(context, "Plato Eliminado correctamente", Toast.LENGTH_SHORT).show()
                }
        }

        navegarHaciaPrincipal()
    }

    private fun mostrarPlato () {
        val idPlato : String = menuDiarioViewModel.platoElegido.id.toString()
        val nombrePlato : String = menuDiarioViewModel.platoElegido.nombre.toString()
        val descPlato : String = menuDiarioViewModel.platoElegido.descripcion.toString()
        val tipoPlato :  String = menuDiarioViewModel.platoElegido.tipo.toString()

        FirebaseStorage.getInstance().reference.child("images/$idPlato").downloadUrl.addOnSuccessListener { uri ->

            Glide
                .with(context)
                .load(uri)
                .into(imagePlatoDelete)

        }. addOnFailureListener {
            Toast.makeText(context, "No se pudo visualizar el Plato", Toast.LENGTH_SHORT).show()
        }

        binding.textNombreEliminar.setText(nombrePlato)
        binding.textDescripcionEliminar.setText(descPlato)
        if (tipoPlato == "null") {
            binding.textTipoPlato.setText("Vianda")
        } else {
            binding.textTipoPlato.setText(tipoPlato)
        }
    }

    private fun cargarMenusAlmuerzo() {
        database = FirebaseDatabase.getInstance().reference

        database.child("menus").child("almuerzo").addListenerForSingleValueEvent(object :
            ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("DBFirebase", "Error de lectura: $databaseError")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()){
                    Log.i("menudiario", dataSnapshot.toString())
                    dataSnapshot.children.forEach {
                            plato -> menuDiarioViewModel.addPlato(plato)
                    }
                    //Observar el arreglo para no bindear desde aca
                    binding.platoDeletespinner.adapter  = ArrayAdapter(requireContext() , R.layout.support_simple_spinner_dropdown_item , menuDiarioViewModel.platos)
                }
            }
        })
    }

    private fun cargarMenusDesayuno() {
        database = FirebaseDatabase.getInstance().reference

        database.child("menus").child("desayuno").addListenerForSingleValueEvent(object :
            ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("DBFirebase", "Error de lectura: $databaseError")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()){
                    Log.i("menudiario", dataSnapshot.toString())
                    dataSnapshot.children.forEach {
                            plato -> menuDiarioViewModel.addPlato(plato)
                    }
                    //Observar el arreglo para no bindear desde aca
                    binding.platoDeletespinner.adapter  = ArrayAdapter(requireContext() , R.layout.support_simple_spinner_dropdown_item , menuDiarioViewModel.platos)
                }
            }
        })
    }

    private fun cargarViandas() {
        database = FirebaseDatabase.getInstance().reference

        database.child("viandas").addListenerForSingleValueEvent(object :
            ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("DBFirebase", "Error de lectura: $databaseError")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()){
                    Log.i("menudiario", dataSnapshot.toString())
                    dataSnapshot.children.forEach {
                            plato -> menuDiarioViewModel.addPlato(plato)
                    }
                    //Observar el arreglo para no bindear desde aca
                    binding.platoDeletespinner.adapter  = ArrayAdapter(requireContext() , R.layout.support_simple_spinner_dropdown_item , menuDiarioViewModel.platos)
                }
            }
        })
    }

    private fun cargarMenusMeriendas() {
        database = FirebaseDatabase.getInstance().reference

        database.child("menus").child("merienda").addListenerForSingleValueEvent(object :
            ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("DBFirebase", "Error de lectura: $databaseError")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()){
                    Log.i("menudiario", dataSnapshot.toString())
                    dataSnapshot.children.forEach {
                            plato -> menuDiarioViewModel.addPlato(plato)
                    }
                    //Observar el arreglo para no bindear desde aca
                    binding.platoDeletespinner.adapter  = ArrayAdapter(requireContext() , R.layout.support_simple_spinner_dropdown_item , menuDiarioViewModel.platos)
                }
            }
        })
    }

    private fun initSpiner() {
        //binding.platoSpinner.adapter  = ArrayAdapter(requireContext() , android.R.layout.simple_spinner_item, menuDiarioViewModel.platos)
        binding.platoDeletespinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                menuDiarioViewModel.platoElegido = menuDiarioViewModel.platos[p2]
            }
        }
    }

    private fun navegarHaciaPrincipal() {
        findNavController().navigate(R.id.action_eliminar_plato_dest_to_pedidos_dest)
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment registro_vianda_dest.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            registro_vianda_dest()
    }

}
