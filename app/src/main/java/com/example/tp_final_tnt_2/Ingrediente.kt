package com.example.tp_final_tnt_2

data class Ingrediente(
    var name:String? = "",
    var description:String? = "",
    var quantity:Int = 0,
    var minimun_stock_level: Int = 0,
    var id:String? = ""
) {
    override fun toString(): String {
        return name.toString()
    }
}