package com.example.tp_final_tnt_2

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.tp_final_tnt_2.databinding.FragmentRegistroPlatoDestBinding
import com.example.tp_final_tnt_2.databinding.FragmentRegistroViandaDestBinding
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.fragment_registro_plato_dest.*
import kotlinx.android.synthetic.main.fragment_registro_vianda_dest.*
import java.text.SimpleDateFormat
import java.util.*



/**
 * A simple [Fragment] subclass.
 * Use the [registro_vianda_dest.newInstance] factory method to
 * create an instance of this fragment.
 */
class registro_vianda_dest : Fragment() {

    private lateinit var _binding: FragmentRegistroViandaDestBinding
    private val binding get() = _binding!!
    private lateinit var database: DatabaseReference
    private val restaurarViandaViewModel: RestaurarViandaViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        cargarViandas()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        this._binding = FragmentRegistroViandaDestBinding.inflate(inflater, container, false)
        val  view = binding.root
        initSpinner()
        binding.registrarVianda.setOnClickListener { registrarViandaEnBD() }
        binding.visualizarButton.setOnClickListener { visualizarVianda() }
        return view
    }

    private fun visualizarVianda() {
        val idPlato : String = restaurarViandaViewModel.viandaElegida.id.toString()
        val nombrePlato : String = restaurarViandaViewModel.viandaElegida.nombre.toString()
        val descPlato : String = restaurarViandaViewModel.viandaElegida.descripcion.toString()

        FirebaseStorage.getInstance().reference.child("images/$idPlato").downloadUrl.addOnSuccessListener { uri ->

            Glide
                .with(context)
                .load(uri)
                .into(imageViewVianda)

        }. addOnFailureListener {
            Toast.makeText(context, "No se pudo visualizar el Plato", Toast.LENGTH_SHORT).show()
        }

        binding.textViewNombre.setText(nombrePlato)
        binding.textViewDescripcion.setText(descPlato)
    }

    private fun cargarViandas() {
        database = FirebaseDatabase.getInstance().reference

        database.child("viandas").addListenerForSingleValueEvent(object :
            ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("DBFirebase", "Error de lectura: $databaseError")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()){
                    Log.i("viandas", dataSnapshot.toString())
                    dataSnapshot.children.forEach {
                            vianda -> restaurarViandaViewModel.addVianda(vianda)
                    }
                    //Observar el arreglo para no bindear desde aca
                    binding.descripcionVianda.adapter  = ArrayAdapter(requireContext() , android.R.layout.simple_spinner_dropdown_item , restaurarViandaViewModel.viandas)
                }
            }
        })
    }

    private fun initSpinner() {
        binding.descripcionVianda.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                restaurarViandaViewModel.viandaElegida = restaurarViandaViewModel.viandas[p2]
            }
        }
    }

    private fun registrarViandaEnBD() {
        database = FirebaseDatabase.getInstance().reference
        val vianda = obtenerVianda()
        val idVianda = vianda.id.toString()
        database.child("viandas").child(idVianda).setValue(vianda) //child("fecha").setValue(fecha)
            .addOnFailureListener{
                Toast.makeText(context, "No se pudo registrar la Vianda", Toast.LENGTH_SHORT).show()
            }
            .addOnSuccessListener {
                Toast.makeText(context, "Vianda cargada correctamente", Toast.LENGTH_SHORT).show()
        }
        navegarHaciaPrincipal()
    }

    private fun navegarHaciaPrincipal() {
        findNavController().navigate(R.id.action_registro_vianda_dest_to_pedidos_dest)
    }

    private fun obtenerVianda(): Vianda {
        val vianda = restaurarViandaViewModel.viandaElegida
        vianda.fecha = obtenerFecha()
        vianda.dia = obtenerDia()
        return vianda
    }

    private fun obtenerDia(): String {
        return when (radioGroupVianda.getCheckedRadioButtonId()){
            R.id.lunes -> "lunes"
            R.id.martes -> "martes"
            R.id.miercoles -> "miercoles"
            R.id.jueves -> "jueves"
            else -> "viernes"
        }
    }

    private fun obtenerFecha():String {
        val sdf = SimpleDateFormat("dd/M/yyyy", Locale.getDefault())
        val currentDate = sdf.format(Date())
        return currentDate
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment registro_vianda_dest.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            registro_vianda_dest()
    }
}