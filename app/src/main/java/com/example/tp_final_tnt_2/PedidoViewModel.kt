package com.example.tp_final_tnt_2

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ktx.getValue

class PedidoViewModel: ViewModel() {
    private val _pedidos: MutableLiveData<MutableList<Pedido>> = MutableLiveData(mutableListOf<Pedido>())
    val pedidos: LiveData<MutableList<Pedido>> get() = _pedidos

    fun addPedidos(dataSnapshot: DataSnapshot) {
        this._pedidos.value = mutableListOf()
        dataSnapshot.children.forEach {
            addPedido(it)
        }
    }

    private fun addPedido(pedidoSnapShot: DataSnapshot) {
        val pedido = getPedidoFromSnapShot(pedidoSnapShot)
        if (pedidoExisteAndNoEstaCompletado(pedido)) return
        if (deboEliminarPedidoCompletado(pedido)) {
            eliminarPedidoCompletado(pedido)
            return
        }
        val lista = _pedidos.value
        lista?.add(pedido)
        this._pedidos.postValue(lista)
    }

    private fun eliminarPedidoCompletado(pedido: Pedido) {
        val lista = _pedidos.value?.filter { p -> p.key != pedido.key } as MutableList
        _pedidos.value = lista
    }

    private fun deboEliminarPedidoCompletado(pedido: Pedido): Boolean {
        return  pedidoExiste(pedido) and (pedido.completo as Boolean)
    }

    private fun pedidoExisteAndNoEstaCompletado(pedido: Pedido): Boolean {
        val noCompletado = !(pedido.completo as Boolean)
        return (pedidoExiste(pedido) && noCompletado)
    }
    private fun pedidoExiste(pedido: Pedido): Boolean {
        return _pedidos.value?.filter { p -> p.key == pedido.key }?.isNotEmpty()!!
    }

    private fun getPedidoFromSnapShot(pedidoSnapShot: DataSnapshot): Pedido {
        val key = pedidoSnapShot.key
        val completo = pedidoSnapShot.child("completo").getValue(Boolean::class.java) as Boolean
        val direccionEntrega = pedidoSnapShot.child("direccionEntrega").getValue(String::class.java).toString()
        val nombre = pedidoSnapShot.child("nombreCliente").getValue(String::class.java).toString()
        val precio= pedidoSnapShot.child("precio").getValue(Double::class.java)//?.toDouble()
        val tipoEntrega = pedidoSnapShot.child("entregaStr").getValue(String::class.java).toString()

        val listaPlatos = mutableListOf<Map<String,Int>>()
        pedidoSnapShot.child("listaPlatos").children.forEach {
            val nombrePlato = it.key as String
            val cant = it.getValue(Int::class.java) as Int
            listaPlatos.add(mapOf(nombrePlato to cant))
        }

        return Pedido(key = key, completo = completo,
            direccion = direccionEntrega, listaPlato = listaPlatos ,
            nombre = nombre, precio = precio, tipoEntrega = tipoEntrega  )
    }
}