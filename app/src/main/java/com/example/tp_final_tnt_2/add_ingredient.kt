package com.example.tp_final_tnt_2

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.tp_final_tnt_2.databinding.FragmentAddIngredientBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [add_ingredient.newInstance] factory method to
 * create an instance of this fragment.
 */
class add_ingredient : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var _binding: FragmentAddIngredientBinding
    private val binding get() = _binding!!
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        this._binding = FragmentAddIngredientBinding.inflate(inflater, container, false)
        val  view = binding.root
        binding.ingredientRegister.setOnClickListener { registerIngredientDB() }
        return view
    }

    private fun registerIngredientDB() {
        database = FirebaseDatabase.getInstance().reference
        val ingredient = assembleIngredient()
        val key = database.child("ingredientes").push().key
        ingredient.id = key
        if (key == null) {
            Log.i("BDFirebase", "No pude pushear clave de ingredientes")
            return
        }

        database.child("ingredientes").child(key).setValue(ingredient) //child("fecha").setValue(fecha)
            .addOnFailureListener{
                Toast.makeText(context, "No se pudo registrar el Ingrediente", Toast.LENGTH_SHORT).show()
            }
            .addOnSuccessListener {
                Toast.makeText(context, "Ingrediente cargada correctamente", Toast.LENGTH_SHORT).show()
            }
        navegarHaciaPrincipal()
    }

    private fun assembleIngredient() :Ingrediente {
        val name = _binding.ingredientName.text.toString()
        val description = _binding.ingredientDescription.text.toString()
        val quantity = _binding.ingredientQuantity.text.toString().toInt()
        val minimun_stock_level = _binding.ingredientMinimunStockLevel.text.toString().toInt()
        return Ingrediente(name, description, quantity, minimun_stock_level)
    }

    private fun navegarHaciaPrincipal() {
        findNavController().navigate(R.id.action_add_ingredient_to_pedidos_dest)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment add_ingredient.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            add_ingredient().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}