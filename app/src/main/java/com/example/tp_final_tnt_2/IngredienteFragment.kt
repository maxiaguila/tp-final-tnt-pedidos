package com.example.tp_final_tnt_2

import android.app.NotificationManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tp_final_tnt_2.databinding.FragmentIngredientesBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.database.*
import com.google.firebase.database.R
import kotlinx.android.synthetic.main.dialogo_ingrdiente.view.*
import kotlinx.android.synthetic.main.dialogo_stock.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * A fragment representing a list of Items.
 */
class IngredienteFragment : Fragment() {


    private lateinit var _binding: FragmentIngredientesBinding
    private val binding get() = _binding
    private val ingredienteViewModel: IngredienteViewModel by activityViewModels()
    lateinit var recyclerView: RecyclerView
    private lateinit var database: DatabaseReference
    private lateinit var materialAlertDialogBuilder: MaterialAlertDialogBuilder
    private lateinit var modalStockView : View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDatabase()
        materialAlertDialogBuilder = MaterialAlertDialogBuilder(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentIngredientesBinding.inflate(inflater, container, false)
        _binding.ingredienteViewModel = ingredienteViewModel
        this.realizarBindings()
        return _binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()
        this.binding.ingredienteViewModel?.ingredientes?.observe(viewLifecycleOwner,
            Observer { ingredientes -> (recyclerView.adapter as IngredienteAdapter).setIngredientes(ingredientes) }
        )
    }


    fun initDatabase() {
        database = FirebaseDatabase.getInstance().reference
        GlobalScope.launch { initIngredientesDesdeBD() }
    }

    suspend private fun initIngredientesDesdeBD() {
        database.child("ingredientes").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("DBFirebase", "Error de lectura: $databaseError")
                mostrarMensaje("No pudimos traer los ingredientes. Vuelve a iniciar al app")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    crearListaIngredientes(dataSnapshot)
                    ingredienteViewModel.ingredientes_cargados = true
                }
            }
        })
    }

    fun crearListaIngredientes(dataSnapshot: DataSnapshot){
        val ingredientes = dataSnapshot.children
        ingredientes.forEach {
            val ingrediente = it.getValue(Ingrediente::class.java)
            ingredienteViewModel.addIngrediente(ingrediente as Ingrediente)
        }
    }

    private fun initRecyclerView() {
        recyclerView = binding.ingredientesRecyclerview
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = getAdapter()
    }

    fun getAdapter() : IngredienteAdapter {

        return IngredienteAdapter( object:IngredienteListener{
            override fun onClickIngrediente(ingrediente: Ingrediente) {
                ingredienteViewModel.seleccionarIngrediente(ingrediente)
                modalStockView = LayoutInflater.from(context).inflate(com.example.tp_final_tnt_2.R.layout.dialogo_stock,null,false)
                editar_stock()
            }
        })
    }

    private fun editar_stock(){
        var ingrediente = ingredienteViewModel.ingrediente_seleccionado
        var nuevo_stock = modalStockView.nuevo_stock
        MaterialAlertDialogBuilder(context)
            .setView(modalStockView)
            .setTitle("${ingrediente.name}")
            .setPositiveButton("Modificar"){ dialog, _ ->
                val cant = nuevo_stock.text.toString().toInt()
                database.child("ingredientes").child(ingrediente.id.toString()).child("minimun_stock_level").setValue(cant)
                    .addOnSuccessListener { mostrarMensaje("Has modificado el stock minimo de ${ingrediente.name} a ${cant}") }
                dialog.dismiss()
            }
            .setNegativeButton("Cancelar") { dialog, _ ->
                dialog.dismiss()
            }
            .show()

    }

    private fun realizarBindings(){
        binding.btnVerificarStock.setOnClickListener { verificar_stock() }
        binding.btnCrearIngrediente.setOnClickListener { navegar_hacia_crear_ingrediente() }
    }

    private fun mostrarMensaje(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }

    private fun verificar_stock() {
        var ingredientes_faltantes = ingredienteViewModel.get_ingredientes_faltantes()
        if (ingredientes_faltantes.isEmpty()) return mostrarMensaje("Su stock de ingredientes es suficiente")
        var mensaje = ""
        ingredientes_faltantes.forEach { it -> mensaje+= "» Stock de ${it.first}: ${it.second} « "}
        notificar_stock_minimo(mensaje)
    }
    private fun notificar_stock_minimo(mensaje: String) {
        val notificationManager = context?.let {
            ContextCompat.getSystemService(
                it,
                NotificationManager::class.java
            )
        } as NotificationManager

        notificationManager.dispararNotificacionStock(mensaje, requireContext())

    }

    fun navegar_hacia_crear_ingrediente(){
        findNavController().navigate(com.example.tp_final_tnt_2.R.id.action_ingredienteFragment_to_add_ingredient)
    }
}