package com.example.tp_final_tnt_2

interface PedidoListener {
    fun enviarPedidoClicked(pedido: Pedido)
    fun verDetallePedidoClicked(pedido: Pedido)
}