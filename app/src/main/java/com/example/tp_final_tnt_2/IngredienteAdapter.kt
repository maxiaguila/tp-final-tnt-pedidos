package com.example.tp_final_tnt_2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class IngredienteAdapter(private val ingredienteListener: IngredienteListener
) : RecyclerView.Adapter<IngredienteAdapter.ViewHolder>() {

    private lateinit var ingredientes: List<Ingrediente>

    internal fun setIngredientes(ingredientes: List<Ingrediente>){
        this.ingredientes = ingredientes
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_detalle_ingrediente, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val ingrediente = this.ingredientes[position]
        holder.name.text = ingrediente.name
        holder.description.text = ingrediente.description
        holder.quantity.text = "En stock: ${ingrediente.quantity}"
        holder.minimun_stock_level.text = "Stock minimo: ${ingrediente.minimun_stock_level}"
        holder.view.setOnClickListener { ingredienteListener.onClickIngrediente(ingrediente) }
    }

    override fun getItemCount(): Int = ingredientes.size


    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val name: TextView
        val description: TextView
        val quantity: TextView
        val minimun_stock_level: TextView

        init {
            name = view.findViewById(R.id.ingrediente_name)
            description = view.findViewById(R.id.ingrediente_description)
            quantity = view.findViewById(R.id.ingrediente_quantity)
            minimun_stock_level = view.findViewById(R.id.ingrediente_stock)
        }

        override fun toString(): String {
            return super.toString() + " '" + name.text + "'"
        }
    }
}