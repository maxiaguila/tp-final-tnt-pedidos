package com.example.tp_final_tnt_2

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tp_final_tnt_2.Ingrediente
import java.util.*

class IngredienteViewModel: ViewModel() {
    private val _ingredientes: MutableLiveData<MutableList<Ingrediente>> = MutableLiveData(mutableListOf<Ingrediente>())
    val ingredientes: LiveData<MutableList<Ingrediente>>
        get() = _ingredientes
    lateinit var ingrediente_seleccionado: Ingrediente
    var ingredientes_cargados = false

    fun seleccionarIngrediente(ingrediente: Ingrediente) {
        ingrediente_seleccionado = ingrediente
    }

    fun addIngrediente(nuevoIngrediente: Ingrediente) {
        if (ingredientes.value?.contains(nuevoIngrediente)!!) return
        ingredientes.value!!.add(nuevoIngrediente)
    }

    fun get_ingredientes_faltantes(): List<Pair<String, Int>> {
        var ingredientes_faltantes = mutableMapOf<String,Int>()
        ingredientes.value?.forEach { i -> if (i.quantity < i.minimun_stock_level) ingredientes_faltantes.put(i.name.toString(), i.quantity) }
        return ingredientes_faltantes.toList()
    }
}

