package com.example.tp_final_tnt_2

import androidx.lifecycle.ViewModel
import com.google.firebase.database.DataSnapshot

class MenuDiarioViewModel: ViewModel() {
    var platos = emptyArray<Plato>()
    var platoElegido: Plato = Plato()

    fun addPlato(platoSnapshot: DataSnapshot) {
        val plato = getPlatoFromDataSnapshot(platoSnapshot)
        if (platos.contains(plato)) return
        this.platos = this.platos.plus(plato)
    }

    private fun getPlatoFromDataSnapshot(platoSnapshot: DataSnapshot): Plato {
        val nombre = platoSnapshot.child("nombre").getValue(String::class.java).toString()
        val descripcion = platoSnapshot.child("descripcion").getValue(String::class.java).toString()
        val precio= platoSnapshot.child("precio").getValue(String::class.java).toString()
        val tipo = platoSnapshot.child("tipo").getValue(String::class.java).toString()
        val fecha = platoSnapshot.child("fecha").getValue(String::class.java).toString()
        return Plato(id = platoSnapshot.key, nombre = nombre, descripcion = descripcion, precio = precio, tipo = tipo, fecha = fecha)
    }

}