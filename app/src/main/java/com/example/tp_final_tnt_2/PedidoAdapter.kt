package com.example.tp_final_tnt_2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.pedido_item.view.*

class PedidoAdapter(var pedidoListener:PedidoListener): RecyclerView.Adapter<PedidoAdapter.PedidoViewHolder>() {
    private lateinit var pedidos:MutableList<Pedido>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PedidoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.pedido_item, parent, false)
        return PedidoViewHolder(view, pedidoListener)
    }

    override fun getItemCount(): Int = pedidos.size

    fun setPedidos(pedidos: MutableList<Pedido>) {
        this.pedidos = pedidos
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: PedidoViewHolder, position: Int) {
        val pedido = this.pedidos[position]
        holder.tipoEntrega.text = pedido.tipoEntrega
        holder.direccion.text = pedido.direccion
        holder.nombre.text = pedido.nombre
        holder.btnEnviar.setOnClickListener { pedidoListener.enviarPedidoClicked(pedido) }
        holder.btndetalle.setOnClickListener { pedidoListener.verDetallePedidoClicked(pedido) }
    }

    inner class PedidoViewHolder(itemView:View, pedidoListener:PedidoListener):RecyclerView.ViewHolder(itemView){
        private val vista:View = itemView
        val nombre: TextView
        val tipoEntrega: TextView
        var direccion: TextView
        var btnEnviar: Button
        var btndetalle: Button

        init {
            nombre = vista.idNombre
            tipoEntrega = vista.tipoEntrega
            direccion = vista.idDireccion
            btnEnviar = vista.enviar_chip
            btndetalle = vista.detalle_chip
        }

    }

}