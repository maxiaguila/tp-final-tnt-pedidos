package com.example.tp_final_tnt_2

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

private val NOTIFICATION_ID = 123
private const val CHANNEL_ID = "canal_chef_juan"
private const val CHANNEL_NOMBRE = "Chef Juan"

fun crearCanal(context: Context) {
    // Crear canal de notificacion para versiones superiores a API 26.
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val canalNotificacion = NotificationChannel(
            CHANNEL_ID.toString(),
            CHANNEL_NOMBRE.toString(),
            NotificationManager.IMPORTANCE_HIGH
        ).apply {
                setShowBadge(true)
                enableLights(true)
                lightColor = Color.RED
                enableVibration(true)
                description = "Canal de notificación de La cocina del chef Juan"
            }
        // Registrar el canal
        val notificationManager: NotificationManager? = context.getSystemService(NotificationManager::class.java)
        notificationManager?.createNotificationChannel(canalNotificacion)
    }
}


fun NotificationManager.dispararNotificacionStock(mensaje: String, applicationContext: Context) {
    val contentIntent = Intent(applicationContext, MainActivity::class.java)
    val contentPendingIntent = PendingIntent.getActivity(
        applicationContext,
        NOTIFICATION_ID,
        contentIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )

    val builder = NotificationCompat.Builder(applicationContext,
        "canal_chef_juan")
        .setSmallIcon(R.drawable.ic_gorro_de_cocinero)
        .setContentTitle("La cocina del Chef Juan: Stock faltante!")
        .setContentText(mensaje)
        .setPriority(NotificationCompat.PRIORITY_HIGH)
        .setContentIntent(contentPendingIntent)
        .setAutoCancel(true)

    with(NotificationManagerCompat.from(applicationContext)) {
        notify(NOTIFICATION_ID, builder.build())
    }
}
