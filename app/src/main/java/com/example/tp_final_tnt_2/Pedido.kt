package com.example.tp_final_tnt_2

data class Pedido (
    var key:String? = "",
    var completo:Boolean? = false,
    var direccion:String? = "",
    var listaPlato: MutableList<Map<String,Int>>? = mutableListOf(),
    var nombre:String? = "",
    var precio: Double? = 0.0,
    var tipoEntrega:String? = ""
)

