package com.example.tp_final_tnt_2

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.tp_final_tnt_2.databinding.FragmentPedidosDestBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_pedidos_dest.*


/**
 * A simple [Fragment] subclass.
 * Use the [pedidos_dest.newInstance] factory method to
 * create an instance of this fragment.
 */
@Suppress("UNREACHABLE_CODE")
class pedidos_dest : Fragment() {

    private lateinit var _binding: FragmentPedidosDestBinding
    private val binding get() = _binding
    private lateinit var database: DatabaseReference
    lateinit var recyclerView : RecyclerView
    val pedidoViewModel: PedidoViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        database = FirebaseDatabase.getInstance().reference
        database.child("pedidos").orderByChild("completo").equalTo(false).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("DBFirebase", "Error de lectura: $databaseError")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()){
                    pedidoViewModel.addPedidos(dataSnapshot)
                }
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        this._binding = FragmentPedidosDestBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.pedidosViewModel = pedidoViewModel
        binding.registroPlato.setOnClickListener { navegarHaciaRegistroPlato() }
        binding.registroVianda.setOnClickListener { navegarHaciaRegistroVianda() }
        binding.restoreMenu.setOnClickListener { navegarHaciaRestoreMenu() }
        binding.eliminarPlato.setOnClickListener { navegarHaciaEliminar() }
        binding.addIngredient.setOnClickListener { navegarHaciaIngrediente() }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()
        pedidoViewModel.pedidos.observe(viewLifecycleOwner, Observer {pedidos ->
            (recyclerView.adapter as PedidoAdapter).setPedidos(pedidos)
        })
    }

    private fun initRecyclerView() {
        recyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = getPedidoAdapter()
    }

    private fun getPedidoAdapter(): PedidoAdapter {
        return PedidoAdapter(object: PedidoListener{
            override fun enviarPedidoClicked(pedido: Pedido) {
                database.child("pedidos").child(pedido.key.toString()).child("completo").setValue(true)
                    .addOnSuccessListener { Toast.makeText(context, "Pedido Listo!",Toast.LENGTH_SHORT).show() }
                    .addOnFailureListener { Toast.makeText(context, "Vuelve a intentarlo",Toast.LENGTH_SHORT).show() }
            }

            override fun verDetallePedidoClicked(pedido: Pedido) {
                val items = pedido.listaPlato?.let { getArrayPedidos(it) }
                items?.let { crearDialogo(it) }
            }
        } )
    }

    private fun getArrayPedidos(ordenes: MutableList<Map<String,Int>>): Array<String> {
        var items = emptyArray<String>()
        ordenes.forEach {
            it.keys.forEach{ clave ->
                items = items.plus("${clave} - ${it.get(clave).toString()}")}
        }
        return items
    }

    private fun crearDialogo(items: Array<String>) {
        MaterialAlertDialogBuilder(context)
            .setTitle("Platos del pedido")
            .setItems(items) { _ , _ -> }
            .setPositiveButton("Cerrar") { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    private fun navegarHaciaRegistroPlato() {
        findNavController().navigate(R.id.action_pedidos_dest_to_registro_plato_dest)
    }

    private fun navegarHaciaRegistroVianda() {
        findNavController().navigate(R.id.action_pedidos_dest_to_registro_vianda_dest)
    }

    private fun navegarHaciaRestoreMenu() {
        findNavController().navigate(R.id.action_pedidos_dest_to_restaurar_menu_diario_dest)
    }

    private fun navegarHaciaEliminar() {
        findNavController().navigate(R.id.action_pedidos_dest_to_eliminar_plato_dest)
    }

    private fun navegarHaciaIngrediente() {
        findNavController().navigate(R.id.action_pedidos_dest_to_ingredient)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment pedidos_dest.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() = pedidos_dest()

    }
}