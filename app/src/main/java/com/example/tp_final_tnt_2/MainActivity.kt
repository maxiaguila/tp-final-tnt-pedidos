package com.example.tp_final_tnt_2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.tp_final_tnt_2.databinding.ActivityMainBinding
import com.google.firebase.database.DatabaseReference


class MainActivity : AppCompatActivity() {

    private lateinit var vista: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        crearCanal(this)

    }

}