package com.example.tp_final_tnt_2

import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.example.tp_final_tnt_2.databinding.FragmentRestaurarMenuDiarioDestBinding
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.fragment_restaurar_menu_diario_dest.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Use the [restaurar_menu_diario_dest.newInstance] factory method to
 * create an instance of this fragment.
 */
class restaurar_menu_diario_dest : Fragment() {

    private lateinit var _binding: FragmentRestaurarMenuDiarioDestBinding
    private val binding get() = _binding
    private lateinit var database: DatabaseReference
    private val menuDiarioViewModel: MenuDiarioViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        cargarMenus()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        this._binding = FragmentRestaurarMenuDiarioDestBinding.inflate(inflater, container, false)
        initSpiner()
        binding.cargarMenuDiarioButton.setOnClickListener { onClickCargarMenu() }
        binding.seleccionarFoto.setOnClickListener { onClickMostrarFoto() }
        return binding.root
    }

    private fun onClickCargarMenu() {
        val idPlato : String = menuDiarioViewModel.platoElegido.id.toString()
        val fecha = obtenerFechaDeHoy()
        database.child("menus").child("almuerzo").child(idPlato).child("fecha").setValue(fecha)
            .addOnFailureListener { Toast.makeText(context, "No se pudo registrar el Plato", Toast.LENGTH_SHORT).show() }
            .addOnSuccessListener { Toast.makeText(context, "Plato cargado correctamente", Toast.LENGTH_SHORT).show() }
    }

    private fun onClickMostrarFoto() {
        val idPlato : String = menuDiarioViewModel.platoElegido.id.toString()
        val nombre : String = menuDiarioViewModel.platoElegido.nombre.toString()
        val descripcion : String = menuDiarioViewModel.platoElegido.descripcion.toString()
        val tipo : String = menuDiarioViewModel.platoElegido.tipo.toString()

        FirebaseStorage.getInstance().reference.child("images/$idPlato").downloadUrl.addOnSuccessListener { uri ->

            Glide
            .with(context)
            .load(uri)
            .into(imagePlato)

        }. addOnFailureListener {
            Toast.makeText(context, "No se pudo visualizar el Plato", Toast.LENGTH_SHORT).show()
        }

        binding.textNombre.setText(nombre)
        binding.textDescripcion.setText(descripcion)
        binding.textTipo.setText(tipo)

    }

    private fun obtenerFechaDeHoy():String {
        val sdf = SimpleDateFormat("dd/M/yyyy", Locale.getDefault())
        val currentDate = sdf.format(Date())
        return currentDate
    }

    private fun cargarMenus() {
        database = FirebaseDatabase.getInstance().reference

        database.child("menus").child("almuerzo").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("DBFirebase", "Error de lectura: $databaseError")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()){
                    Log.i("menudiario", dataSnapshot.toString())
                    dataSnapshot.children.forEach {
                            plato -> menuDiarioViewModel.addPlato(plato)
                    }
                    //Observar el arreglo para no bindear desde aca
                    binding.platoSpinner.adapter  = ArrayAdapter(requireContext() , android.R.layout.simple_spinner_dropdown_item , menuDiarioViewModel.platos)
                }
            }
        })

    }

    private fun initSpiner() {
        //binding.platoSpinner.adapter  = ArrayAdapter(requireContext() , android.R.layout.simple_spinner_item, menuDiarioViewModel.platos)
        binding.platoSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                menuDiarioViewModel.platoElegido = menuDiarioViewModel.platos[p2]
            }
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment restaurar_menu_diario_dest.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            restaurar_menu_diario_dest().apply {
                arguments = Bundle().apply {

                }
            }
    }
}

